
CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation
  * Configuration


INTRODUCTION
------------

Current Maintainer: Scot Hubbard - http://drupal.org/user/868214

This module allows admins to set the package groups on the modules admin page
to collapsed by default.


INSTALLATION
------------

Install as usual...
see http://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------

Once installed, use the "configure" link on the modules page to go to
the configuration page for this module.
